from SerialCom.serialcom import SerialCom
import PySimpleGUI as sg
import utils


class Template:
    def __init__(self) -> None:
        self.ttk_style = 'vista'
        sg.theme('SystemDefaultForReal')
        self.first_row_of_telemetry = [
            [sg.T("Telemetria")],
            sg.T("Ciśnienie"),
            sg.Input("", disabled=True, size=(10, 0), key="pressure"),
            sg.T("Temp. Wew."),
            sg.Input("", disabled=True, size=(10, 0), key="in_temp"),
            sg.T("Wilgotność Wew."),
            sg.Input("", disabled=True, size=(10, 0), key="in_humidity"),
            sg.T("X,Y,Z"),
            sg.Input(
                "", disabled=True, size=(20, 0), key="axis"),
        ]
        self.second_row_of_telemetry = [
            sg.T("Głębokość"),
            sg.Input("", disabled=True, size=(10, 0), key="depth"),
            sg.T("Temp zew."),
            sg.Input("", disabled=True, size=(10, 0), key="out_temp"),
            sg.T("Natężenie światła"),
            sg.Input("", disabled=True, size=(10, 0), key="light_intensity"),
        ]

        self.engines = [
            sg.T("Lewy silnik"),
            sg.Input("", disabled=True, size=(10, 0), key="left_engine_pin_1"),
            sg.Input("", disabled=True, size=(10, 0), key="left_engine_pin_2"),
            sg.T("Centralny silnik"),
            sg.Input("", disabled=True, size=(10, 0),
                     key="central_engine_pin_1"),
            sg.Input("", disabled=True, size=(10, 0),
                     key="central_engine_pin_2"),
            sg.T("Prawy silnik"), sg.Input("", disabled=True,
                                           size=(10, 0), key="right_engine_pin_1"),
            sg.Input("", disabled=True, size=(10, 0), key="right_engine_pin_2")
        ]
        self.raw_telemetry = [
            sg.T("Surowa telemetria", key="cos"), sg.Button("Wyczyść", use_ttk_buttons=True, size=(10, 1),
                                                            key=lambda: self.serial.clearTelemetry()),
            [sg.Listbox([""], size=(110, 20),
                        key="raw_serial_monitor")]
        ]
        self.light_and_serial_com = [
            [sg.T("Sterowanie światłem: "), sg.Button("On", use_ttk_buttons=True, size=(10, 1),
                                                      key=lambda: self.serial.sendPacket('L')), sg.T(" / "), sg.Button("Off", use_ttk_buttons=True, size=(10, 1),
                                                                                                                       key=lambda: self.serial.sendPacket('l'))],

            sg.Button("Połącz", use_ttk_buttons=True, size=(10, 1),
                      key=lambda value: self.serial.openSerialPort(value)),

            sg.InputCombo(('Combobox 1', 'Combobox 2'), size=(
                20, 1), key="serial_ports_list", readonly=True),
            sg.Button("Rozłącz", use_ttk_buttons=True, size=(10, 1),
                      key=lambda: self.serial.closeSerialPort())
        ]
        self.controls_buttons = [
            [sg.Button("STOP", use_ttk_buttons=True,  size=(10, 1),
                       key=lambda: self.serial.sendPacket('x'))],
            [sg.Button("⯅", use_ttk_buttons=True, size=(16, 2),
                       key=lambda: self.serial.sendPacket('w'))],
            [
                sg.Button("⯇", use_ttk_buttons=True, size=(4, 2),
                          key=lambda: self.serial.sendPacket('a')),
                sg.Button("⯈", use_ttk_buttons=True, size=(4, 2),
                          key=lambda: self.serial.sendPacket('d')),
                sg.Button("Wynurz", use_ttk_buttons=True,  size=(16, 2),
                          key=lambda: self.serial.sendPacket('j')),
                sg.Button("Zanurz", use_ttk_buttons=True,  size=(16, 2),
                          key=lambda: self.serial.sendPacket('u')),
            ],

            sg.Button("⯆", use_ttk_buttons=True,  size=(16, 2),
                      key=lambda: self.serial.sendPacket('s')),



        ]


class Window(Template):
    def __init__(self) -> None:
        Template.__init__(self)

        self.window = sg.Window('ROV - Control Panel', [[
            [sg.Text('_' * 110)],
            self.raw_telemetry,
            [sg.Text('_' * 110)],
            self.first_row_of_telemetry,
            self.second_row_of_telemetry,
            self.engines,
            [sg.Text('_' * 110)],

        ], [
            [sg.T("Sterowanie oraz komunikacja szeregowa")],
            self.controls_buttons, self.light_and_serial_com
        ]], ttk_theme=self.ttk_style).finalize()
        self.serial = SerialCom(self.window)
        utils.updateSerialPortList(self)
