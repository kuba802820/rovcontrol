from Templates.template import Window
import os
import inspect
import psutil


class App(Window):
    def __init__(self) -> None:
        Window.__init__(self)
        while True:
            event, values = self.window.read(timeout=500)
            if event in (None, 'Exit'):
                current_app_process = os.getpid()
                AppProcess = psutil.Process(current_app_process)
                AppProcess.terminate()
                break
            if callable(event):
                args = inspect.getfullargspec(event).args
                event(values) if len(args) > 0 else event()


if __name__ == '__main__':
    App()
