# Underwater Drone Control Console

## Description
Underwater Drone Control Console is a Python application that allows the user to control an underwater drone using a computer keyboard. The application provides telemetry viewing, camera images, log recording and a simple video recording function.

## Features
- **Drone Control**: Control underwater drone using the keyboard.
- **Telemetry Viewing**: Monitor your drone's parameters in real time.
- **Camera View**: Watch live video from your drone's camera.
- **Log Record**: Automatically save logs from each session.
- **Picture Recording**: A simple tool to record video from your drone's camera.

![](https://i.imgur.com/JXKWaSL.png)