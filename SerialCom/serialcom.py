import threading
import serial.tools.list_ports
import serial
import PySimpleGUI as sg
import time
import os.path
import utils


class SerialCom(threading.Thread):
    def __init__(self, window) -> None:
        self.telemetryTimeout = 1.3
        self.baudrate = 9600
        self.timeout = 0
        self.serialPort = None
        self._stopevent = threading.Event()
        self._sleepperiod = 1.0
        threading.Thread.__init__(self)
        threading.Thread.start(self)
        self.path = os.path.dirname(os.path.realpath(__file__))
        self.nameOfFile = os.path.join(f"{self.path}/logs", "logs.txt")
        self.window = window
        self.recievedFrames = []
        pass

    def openSerialPort(self, value):
        comPort = value.get('serial_ports_list')
        try:
            self.serialPort = serial.Serial(
                port=comPort, baudrate=self.baudrate, timeout=0)
            time.sleep(2)
            self._stopevent.clear()
            threading.Thread.__init__(self)
            threading.Thread.start(self)
            if self.serialPort:
                self.serial = self.serialPort
                self.clearTelemetry()
                sg.Popup(f"Pomyślnie połączono się z portem {comPort}")

        except Exception as e:
            sg.popup_error(f'BŁĄD!', e)

    def closeSerialPort(self):
        if(self.serialPort and self.serialPort.is_open):
            sg.Popup("Pomyślnie rozłączono")
            self.serialPort.close()
            self.killThread()
            self.serialPort = None

        else:
            sg.popup_error("BŁĄD!", "Port nie jest otwarty")

    def sendPacket(self, packet):
        if(self.serialPort and self.serialPort.is_open):
            if(len(packet) > 0):
                self.serialPort.write(packet.encode())

    def clearTelemetry(self):
        self.recievedFrames = []
        self.window["raw_serial_monitor"].Update(
            values=[])
        self.window.Refresh()

    def readDataFromSerial(self):
        if(self.serialPort and self.serialPort.is_open):
            packet = self.serialPort.readline()
            if(len(packet) > 0):
                try:
                    telemetry_frame = str(packet, 'utf-8')
                    if(telemetry_frame.split(';;')[0] == "OK"):
                        utils.telemetryFormat(self, telemetry_frame)
                        self.recievedFrames.append(
                            telemetry_frame.replace("\r\n", ""))
                        self.window["raw_serial_monitor"].Update(
                            values=[frame for frame in reversed(self.recievedFrames)])
                        self.window.Refresh()
                    else:
                        self.recievedFrames.append("FAILED RECIEVED")
                except Exception as e:
                    sg.popup_error(f'BŁĄD!', e)

    def getSerialPorts(self):
        portList = serial.tools.list_ports.comports()
        ports = []
        for el in portList:
            if not None:
                ports.append(el.device)
        return ports

    def run(self):
        while not self._stopevent.isSet():
            self.readDataFromSerial()
            time.sleep(self.telemetryTimeout)
            self.sendPacket('t')  # Send packet for telemetry
            self._stopevent.wait(self._sleepperiod)

    def killThread(self, timeout=None):
        self._stopevent.set()
        threading.Thread.join(self, timeout)
