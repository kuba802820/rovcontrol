

def updateSerialPortList(self):
    ports = self.serial.getSerialPorts()
    self.window["serial_ports_list"].Update(
        values=[port for port in ports])


def telemetryFormat(self, frame):
    str_frame = str(frame).replace('\r\n', '')
    keys = [("out_temp", " *C"), ("axis", ""),
            ("light_intensity", " Lux"), ("pressure", " psi"), ("depth", "m")]
    arr = str_frame.split(';;')
    if(arr[1] == "TELEMETRY"):
        formatedTelemetry = dict(zip(keys, arr[2::]))
        for key in formatedTelemetry:
            name, unit = key
            self.window[name].Update(value=f"{formatedTelemetry[key]}{unit}")
    if(arr[1] == "ENG"):
        keys = ["right_engine_pin_1", "right_engine_pin_2",
                "left_engine_pin_1", "left_engine_pin_2", "central_engine_pin_1", "central_engine_pin_2"]
        formatedEngineTelemetry = dict(zip(keys, list(arr[2])))
        for key in formatedEngineTelemetry:
            self.window[key].Update(value=f"{formatedEngineTelemetry[key]}")
# PP LL CC
